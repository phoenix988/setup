#!/bin/bash

PLUGINS="$HOME/.config/oh-my-zsh/custom/plugins"
GIT_URL="https://github.com"

# install zsh plugins
[ -d "$PLUGINS/zsh-syntax-highlighting" ] || git clone $GIT_URL/zsh-users/zsh-syntax-highlighting.git "$PLUGINS/zsh-syntax-highlighting"
[ -d "$PLUGINS/zsh-autosuggestions" ] || git clone $GIT_URL/zsh-users/zsh-autosuggestions.git "$PLUGINS/zsh-autosuggestions"
[ -d "$PLUGINS/zsh-vim-mode" ] || git clone $GIT_URL/softmoth/zsh-vim-mode.git "$PLUGINS/zsh-vim-mode"
