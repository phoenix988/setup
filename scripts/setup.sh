#!/bin/bash

SETUP_PATH=$HOME/.cache/setup

command_exist() {
        type "$1" &> /dev/null;
}

error() { \
    clear; printf "ERROR:\\n%s\\n" "$1" >&2; exit 1;
}

command_exist git || error "Git is not installed please install it"

message_command () {
    echo $1

    $2 &> /dev/null

    clear
}

[ -d $SETUP_PATH ] || message_command "Starting setup..... Please wait" "git clone https://gitlab.com/phoenix988/setup.git $SETUP_PATH"

cd $SETUP_PATH

bash $SETUP_PATH/main

